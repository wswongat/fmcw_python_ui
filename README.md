# FMCW_python_UI

FMCW radar display @ python

1.Download the source code
https://gitlab.com/wswongat/fmcw_python_ui
or 
git clone https://gitlab.com/wswongat/fmcw_python_ui .

2.Install the dependencies

pip install pyserial

pip install pyqtgraph

pip install PyQt5

3.Run myqt.py

4.Connect the radar to the PC

5.Click the refresh button and select the correct COM port to our radar

6.Click connect button and everything should be fine

Have fun!
