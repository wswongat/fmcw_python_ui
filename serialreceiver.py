import serial
import time
from threading import Thread

fftdata1=[]
fftdata2=[]
fftdata3=[]
fftdata4=[]

class mSerial(Thread):
    def __init__(self,COM,ser=None,baud=6000000):
        global fftdata1
        global fftdata2
        global fftdata3
        global fftdata4
        Thread.__init__(self)
        self.runStatus = True
        print("COM Thread started")
        self.ser=serial.Serial(COM , baud, timeout=0.5)
        self.ser.flushInput()
        self.channel = [0,0,0,0]
        self.enable = [True,True,True,True]
        time.sleep(.2)
        counter = 0
        while counter < 512:
            fftdata1.append(0)
            fftdata2.append(0)
            fftdata3.append(0)
            fftdata4.append(0)
            counter+=1
    def dataSeparator(self,channel,data):
        global fftdata1
        global fftdata2
        global fftdata3
        global fftdata4
        counter = 0
        fftcounter = 0
        while fftcounter < 512:
            temp = int(data[counter]) * 256
            counter += 1
            temp += int(data[counter])
            counter += 1
            if channel == 1:
                fftdata1[fftcounter] = temp
            elif channel == 2:
                fftdata2[fftcounter] = temp
            elif channel == 3:
                fftdata3[fftcounter] = temp
            elif channel == 4:
                fftdata4[fftcounter] = temp
            fftcounter+=1
       
    def run(self):
        while self.runStatus:
            ser = self.ser
            try:
                data = ser.read()
                if data == b'\x46':#'F'
                    data = ser.read()
                    if data == b'\x54':#'T'
                        data = ser.read()
                        if data == b'\x31':#'1'
                            data = ser.read(1026)
                            if data[-1] == 10 and data[-2] == 126:#'\n' & '~'
                                #print("FT1 received")
                                self.channel[0] = 1
                                self.dataSeparator(1, data)
                        elif data == b'\x32':#'2'
                            data = ser.read(1026)
                            if data[-1] == 10 and data[-2] == 126:#'\n' & '~'
                                #print("FT2 received")
                                self.channel[1] = 1
                                self.dataSeparator(2, data)
                        elif data == b'\x33':#'3'
                            data = ser.read(1026)
                            if data[-1] == 10 and data[-2] == 126:#'\n' & '~'
                                #print("FT3 received")
                                self.channel[2] = 1
                                self.dataSeparator(3, data)
                        elif data == b'\x34':#'4'
                            data = ser.read(1026)
                            if data[-1] == 10 and data[-2] == 126:#'\n' & '~'
                                #print("FT4 received")
                                self.channel[3] = 1
                                self.dataSeparator(4, data)
            except KeyboardInterrupt:
                print("COM thread break")
                break
        ser.close()

    def getdata(self,channel):
        global fftdata1
        global fftdata2
        global fftdata3
        global fftdata4
        if channel == 1:
            return fftdata1
        elif channel == 2:
            return fftdata2
        elif channel == 3:
            return fftdata3
        elif channel == 4:
            return fftdata4
    def getChannel(self):
        return self.channel

    def stop(self):
        self.runStatus = False
        self.ser.close()
        fftdata1.clear()
        fftdata2.clear()
        fftdata3.clear()
        fftdata4.clear()
        self.join()
        self._stop()

        
if __name__ == '__main__':
    s1=mSerial('com3',baud=115200) #for windows only
    #s1=mSerial('/dev/ttyUSB0') #for linux only
    s1.start()
    while True:
        try:
            time.sleep(1)
        except KeyboardInterrupt:
            break
    import os
    os._exit(0)
