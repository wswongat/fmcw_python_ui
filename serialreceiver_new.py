import serial
import time
from threading import Thread

fftdata1=[]
fftdata2=[]
fftdata3=[]
fftdata4=[]

class mSerial(Thread):
    def __init__(self,COM,ser=None,baud=6000000):
        global data1
        global data2
        global data3
        global data4
        Thread.__init__(self)
        self.runStatus = True
        print("COM Thread started")
        self.ser=serial.Serial(COM , baud, timeout=0.5)
        self.ser.flushInput()
        self.channel = [0,0,0,0]
        self.enable = [True,True,True,True]
        time.sleep(.2)
        counter = 0
        while counter < 512:
            fftdata1.append(0)
            fftdata2.append(0)
            fftdata3.append(0)
            fftdata4.append(0)
            counter+=1
    def dataSeparator(self,data):
        global data1
        global data2
        global data3
        global data4

       
    def run(self):
        while self.runStatus:
            ser = self.ser
            try:
                rawdata = ser.readline()
                data = []
                if(rawdata == b'FYP\n'):
                    for i in range(500):
                        data.append(ser.readline().decode()[:-1])
                    print(data)
                self.dataSeparator(data)
            except KeyboardInterrupt:
                print("COM thread break")
                break
        ser.close()

    def getdata(self,channel):
        global data1
        global data2
        global data3
        global data4
        if channel == 1:
            return fftdata1
        elif channel == 2:
            return fftdata2
        elif channel == 3:
            return fftdata3
        elif channel == 4:
            return fftdata4
    def getChannel(self):
        return self.channel

    def stop(self):
        self.runStatus = False
        self.ser.close()
        fftdata1.clear()
        fftdata2.clear()
        fftdata3.clear()
        fftdata4.clear()
        self.join()
        self._stop()

        
if __name__ == '__main__':
    s1=mSerial('com13',baud=2000000) #for windows only
    #s1=mSerial('/dev/ttyUSB0') #for linux only
    s1.start()
    while True:
        try:
            time.sleep(1)
        except KeyboardInterrupt:
            break
    import os
    os._exit(0)
