from pyqtgraph.Qt import QtGui, QtCore
from pyqtgraph.dockarea import *
import numpy as np
import pyqtgraph as pg
from serial.tools import list_ports
from serialreceiver import mSerial
from collections import deque
import csv
import time
import os  

timedata = [deque([]),deque([]),deque([]),deque([])]
IQdata1 = [[],[]]
s1 = None

fileString = ''

app = QtGui.QApplication([])
win = QtGui.QMainWindow()
area = DockArea()
win.setCentralWidget(area)
win.resize(1280, 720)
win.setWindowTitle('FYP project')

d1 = Dock("Dock1", size=(1, 1))  ## give this dock the minimum possible size
area.addDock(
    d1, 'top'
)  ## place d1 at left edge of dock area (it will fill the whole space since there are no other docks yet)
d1.hideTitleBar()
w1 = pg.LayoutWidget()
txtlabel = QtGui.QLabel("24GHz Continuous-Wave Radar @ STM32L4")

d2 = Dock("Dock-FFT", size=(500, 400))
area.addDock(d2, 'bottom')
glw = pg.GraphicsLayoutWidget()
#w2 = pg.PlotItem(title="FFT@1024")

label = pg.LabelItem(justify='right')
titlelabel = pg.LabelItem()
titlelabel.setText("Velocity")
glw.addItem(titlelabel, row=0, col=0)
glw.addItem(label, row=0, col=0)
p1 = glw.addPlot(row=1, col=0)
p1.setYRange(0, 2500, padding=0)
p2 = glw.addPlot(row=2,col=0)
p2_img = pg.ImageItem()
p2.addItem(p2_img)
p2_hist = pg.HistogramLUTItem()
p2_hist.setImageItem(p2_img)
p2_hist.setLevels(0, 2500)
p2.setLimits(xMin=0, xMax=500, yMin=0, yMax=60)
p2_hist.gradient.restoreState(
        {'mode': 'rgb',
         'ticks': [(0.5, (0, 182, 188, 255)),
                   (1.0, (246, 111, 0, 255)),
                   (0.0, (75, 0, 113, 255))]})
p2_img.scale(1,0.25)#1,0.55
p2_hist.setLevels(0, 10000)
p2.setLabel('bottom', "Time", units='sample')
# If you include the units, Pyqtgraph automatically scales the axis and adjusts the SI prefix (in this case kHz)
p2.setLabel('left', "Velocity", units='km/h')

p1.addLegend()
#pg.LegendItem((100,60), offset=(70,30))
d2.addWidget(glw)
# info = QtGui.QGroupBox("Info.")
# vbox = QtGui.QHBoxLayout()
# maxLabel = QtGui.QLabel("MAX:")
# maxValLabel = QtGui.QLabel("9999")
# vbox.addWidget(maxLabel)
# vbox.addWidget(maxValLabel)
# info.setLayout(vbox)
# d2.addWidget(info, row=0, col=1)

d3 = Dock("Dock-Info", size=(100, 100))
d3_w1 = pg.LayoutWidget()
txtlabelSpeed = QtGui.QLabel("Speed(km/h):")
txtlabelSpeedValue = QtGui.QLabel("0")
newfont = QtGui.QFont("Times", 20, QtGui.QFont.Bold) 
newfontBig = QtGui.QFont("Times", 46, QtGui.QFont.Bold) 
txtlabelSpeed.setFont(newfont)
txtlabelSpeedValue.setFont(newfontBig)
d3_w1.addWidget(txtlabelSpeed)
d3_w1.nextRow()
d3_w1.addWidget(txtlabelSpeedValue)
d3.addWidget(d3_w1)
area.addDock(d3, 'right', d2)
# d = {  'Signal Maximum': {
#         '1': 0,
#         '2': 0,
#         '3': 0,
#         '4': 0
#     }
# }

# tree = pg.DataTreeWidget(data=d)
# tree.resize(200, 200)
# #tree.buildTre
# d3.addWidget(tree)

curve = []
curve.append(p1.plot(pen='r', name="Signal 1"))
curve.append(p1.plot(pen='g', name="Signal 2"))
curve.append(p1.plot(pen='b', name="Signal 3"))
curve.append(p1.plot(pen='y', name="Signal 4"))
vLine = pg.InfiniteLine(angle=90, movable=False)
p1.addItem(vLine)

p1Index = 0


def mouseMoved(evt):
    global p1Index
    pos = evt[0]  ## using signal proxy turns original arguments into a tuple
    if p1.sceneBoundingRect().contains(pos):
        mousePoint = p1.vb.mapSceneToView(pos)
        p1Index = int(mousePoint.x())
        #if p1Index > 0 and p1Index < len(data[0]):
        #label.setPos(mousePoint.x()+0.1,mousePoint.y()-0.1)
        #label.setText("<span style='font-size: 12pt'>x=%0.1f,   <span style='color: red'>y1=%0.1f</span>" % (mousePoint.x(), data[0][p1Index]))
        vLine.setPos(mousePoint.x())


proxy = pg.SignalProxy(p1.scene().sigMouseMoved, rateLimit=60, slot=mouseMoved)

def scale(arr):
    arr = np.where(arr > 2500, 2500, arr)
    return arr
def spectrogram(data):
    global p2_img
    p2_img.setImage(data)

def update():
    global curve, p1, p1Index, s1, timedata1,fileString,IQdata1
    fftstep = 0.549316406
    fftfreq = np.arange(0, 120, fftstep)#48.828125    
    text = "<span style=\'font-size: 12pt\'>x=" + str(p1Index)
    for i in range(4):
        if s1.channel[i] and s1.enable[i]:
            temp = s1.getdata(i + 1)
            temp[0] = 0
            temp = temp[0:219]
            if i == 0:
                IQdata1[0] = temp
            if i == 2:
                IQdata1[1] = temp 
            if i == 0:
                with open(fileString, 'a', newline='') as csvfile:
                    writer = csv.writer(csvfile)
                    writeTemp = [[time.strftime("%Y-%m-%d %H:%M", time.localtime())],temp]    
                    writer.writerow(writeTemp)
            temp = np.asarray(temp)
            temp = scale(temp) 
            if i == 0:
                timedata[i].append(temp)
                if len(timedata[i]) >= 300:
                    timedata[i].popleft()
                timeserial = np.asarray(timedata[i])
                spectrogram(timeserial)
                if np.max(temp) > 500:
                    txtlabelSpeedValue.setText(str(round(np.argmax(temp)*fftstep,3)))
                #     d['Signal Maximum'][str(i+1)] = np.argmax(temp)*fftstep
                else:
                    txtlabelSpeedValue.setText("0")    
                #     d['Signal Maximum'][str(i+1)] = 0
            curve[i].setData(y=temp, x=fftfreq)
            if p1Index > 0 and p1Index < fftfreq[-1]:
                text += (",<span>y%d=%0.1f</span>" % (i + 1, temp[int(p1Index / fftstep)]))
        else:
            curve[i].clear()
    #curve[0].setData(data[ptr%10])
    #curve[1].setData(data[ptr%10]+5)
    #p1.enableAutoRange('xy', False)  ## stop auto-scaling after the first data set is plotted
    #tree.setData(d)
    if p1Index > 0 and p1Index < fftfreq[-1]:
        label.setText(text)
        #label.setText("<span style='font-size: 12pt'>x=%0.1f,   <span style='color: red'>y1=%0.1f</span>" % (p1Index, data[ptr%10][p1Index]))


timer = QtCore.QTimer()
timer.timeout.connect(update)

SettingBtn = QtGui.QPushButton('Setting')
SettingBtn.setEnabled(False)
d2.addWidget(SettingBtn)


def settingAction():
    d = QtGui.QDialog()
    d.resize(100, 100)
    CheckBox = []

    def CheckBoxAction():
        i = 0
        for box in CheckBox:
            s1.enable[i] = box.isChecked()
            i += 1

    for i in range(4):
        CheckBox.append(QtGui.QCheckBox("Signal " + str(i + 1), d))
        CheckBox[i].move(5, 5 + i * 20)
        CheckBox[i].setChecked(s1.enable[i])
        CheckBox[i].stateChanged.connect(CheckBoxAction)
    d.setWindowTitle("Setting")
    d.setWindowModality(QtCore.Qt.ApplicationModal)
    d.exec_()


SettingBtn.clicked.connect(settingAction)

comboBox = QtGui.QComboBox()


def updateComList():
    if (len(list_ports.comports())):
        comboBox.clear()
        for item in list_ports.comports():
            comboBox.addItem(item.device)
    else:
        comboBox.clear()


updateComList()

ConnectBtn = QtGui.QPushButton('Connect')
connectStatus = False


def connectAction():
    global s1, connectStatus,fileString
    if not connectStatus and comboBox.currentText() != '':
        import sys
        if 'linux' in sys.platform:
            s1 = mSerial(str(comboBox.currentText()))
        else:
            s1 = mSerial(str(comboBox.currentText()).lower(),baud=460800)
        s1.start()
        ConnectBtn.setText('Disconnect')
        fileString = "output " + str(time.strftime("%Y-%m-%d %H%M%S", time.localtime())) + ".csv"
        timer.start(35)
        SettingBtn.setEnabled(True)
    else:
        if (s1 != None):
            s1.stop()
            timer.stop()
        ConnectBtn.setText('Connect')
        SettingBtn.setEnabled(False)

    connectStatus = not (connectStatus)


ConnectBtn.clicked.connect(connectAction)

RefreshBtn = QtGui.QPushButton('Refresh')


def RefreshAction():
    updateComList()


RefreshBtn.clicked.connect(RefreshAction)

w1.addWidget(txtlabel, row=0, col=0)
w1.addWidget(comboBox, row=0, col=1)
w1.addWidget(RefreshBtn, row=0, col=2)
w1.addWidget(ConnectBtn, row=0, col=3)
d1.addWidget(w1)

win.show()
## Start Qt event loop unless running in interactive mode or using pyside.
if __name__ == '__main__':
    import sys
    if (sys.flags.interactive != 1) or not hasattr(QtCore, 'PYQT_VERSION'):
        QtGui.QApplication.instance().exec_()
